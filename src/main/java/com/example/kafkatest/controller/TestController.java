package com.example.kafkatest.controller;

import com.example.kafkatest.dto.TestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
@RequiredArgsConstructor
@Slf4j
public class TestController {

//    private final KafkaTemplate<String, String> kafkaTemplate;
//
//    @PostMapping("say-something/{text}")
//    public ResponseEntity<String> saySmth(@PathVariable String text) {
//        kafkaTemplate.send("test-topic", text);
//        return ResponseEntity.ok("Success");
//
//    }

    private final KafkaTemplate<String, Object> kafkaTemplate;

    @PostMapping("kafka")
    public ResponseEntity<String> sendDto(@RequestBody TestDto dto) {
        ProducerRecord<String, Object> x = new ProducerRecord<>("test1-topic", 0, "1234l", dto);
        kafkaTemplate.send(x);
        return ResponseEntity.ok("Success");

    }

//    private final KafkaTemplate<String, TestDto> kafkaTemplate;
//
//    @PostMapping("kafka")
//    public ResponseEntity<String> sendDto(@RequestBody TestDto dto) {
//        kafkaTemplate.send("test1-topic", dto);
//        return ResponseEntity.ok("Success");
//
//    }
}

package com.example.kafkatest.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.stereotype.Service;

@Service
public class KafkaTopicService {

    public NewTopic createNewTopic(String name, Integer partition, Integer replica) {
        return TopicBuilder.name(name)
                .partitions(partition)
                .replicas(replica)
                .build();

    }

}

package com.example.kafkatest.config;

import com.example.kafkatest.dto.UserRequestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.DeadLetterPublishingRecoverer;
import org.springframework.kafka.listener.DefaultErrorHandler;
import org.springframework.kafka.support.ExponentialBackOffWithMaxRetries;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class KafkaConsumerConfig {

    private final KafkaTemplate<String, Object> errorTemplate;

    //    private final ErrorHandler errorHandler;
    @Value("${spring.kafka.consumer.bootstrap-servers}")
    String bootStrapServer;

    @Value("${spring.kafka.consumer.group-id}")
    String groupId;

    @Value("${spring.kafka.consumer.interval}")
    Integer interval;

    @Value("${spring.kafka.consumer.retry}")
    Integer retryCount;

    @Value("${spring.kafka.consumer.multiplier}")
    Integer multiplier;

    public ConsumerFactory<String, UserRequestDto> consumerFactory() {
        JsonDeserializer<UserRequestDto> deserializer = new JsonDeserializer<>(UserRequestDto.class);
        deserializer.setRemoveTypeHeaders(false);
        deserializer.addTrustedPackages("*");
        deserializer.setUseTypeMapperForKey(true);
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServer);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(), deserializer);
    }

    @Bean
    public KafkaListenerContainerFactory<?> kafkaListenerContainerFactory() {
        var factory = new ConcurrentKafkaListenerContainerFactory<String, UserRequestDto>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }

    public ConsumerFactory<String, String> consumerFactoryCustom() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServer);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        return new DefaultKafkaConsumerFactory<>(props);
    }

    @Bean
    public KafkaListenerContainerFactory<?> kafkaListenerContainerFactoryCustom() {
        var factory = new ConcurrentKafkaListenerContainerFactory<String, String>();
        factory.setConsumerFactory(consumerFactoryCustom());
        factory.setCommonErrorHandler(errorHandler());

//      factory.setRetryTemplate(retryTemplate());
//      factory.setErrorHandler(errorHandler);

        return factory;
    }

    public DefaultErrorHandler errorHandler() {
        ExponentialBackOffWithMaxRetries exponentialBackOff = new ExponentialBackOffWithMaxRetries(retryCount); // total retries: 1 + 2
        exponentialBackOff.setInitialInterval(interval);
        exponentialBackOff.setMultiplier(multiplier);

        DefaultErrorHandler errorHandler = new DefaultErrorHandler(dltPublishingRecover(), exponentialBackOff);

        errorHandler.setAckAfterHandle(true);
        setNonRetryableExceptions(errorHandler);
        setRetryableExceptions(errorHandler);
        logRetryListener(errorHandler);

        return errorHandler;
    }


    private void setRetryableExceptions(DefaultErrorHandler errorHandler) {
        errorHandler.addRetryableExceptions(
                NullPointerException.class
        );
    }

    private void setNonRetryableExceptions(DefaultErrorHandler errorHandler) {
        errorHandler.addNotRetryableExceptions(
                IllegalArgumentException.class
        );
    }

    private void logRetryListener(DefaultErrorHandler errorHandler) {
        errorHandler.setRetryListeners((consumerRecord, ex, nthAttempt) ->
                log.info("Failed Record - Retry Listener data : {}, exception : {} , nthAttempt : {} ",
                        consumerRecord, ex.getMessage(), nthAttempt)
        );
    }


    public DeadLetterPublishingRecoverer dltPublishingRecover() {
        return new DeadLetterPublishingRecoverer(errorTemplate,
                (failedRecord, e) -> {
                    final Throwable cause = e.getCause();
                    log.error("{}", cause.getMessage());

                    final KafkaErrorMessage<Object> kafkaErrorMessage = KafkaErrorMessage
                            .builder()
                            .data(failedRecord.value())
                            .error(cause.getMessage())
                            .build();
                    errorTemplate.send(getTopic(failedRecord), (String) failedRecord.key(), kafkaErrorMessage);
                    return null;
//                    return new TopicPartition(getTopic(failedRecord), failedRecord.partition());
                });
    }

    private String getTopic(ConsumerRecord<?, ?> rec) {
        return String.format("%s_%s_ERROR", rec.topic(), groupId);
    }


//---------------------------------------------------
//    public RetryTemplate retryTemplate() {
//        final RetryTemplate retryTemplate = new RetryTemplate();
//        final FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
//        backOffPolicy.setBackOffPeriod(1000);
//        retryTemplate.setBackOffPolicy(backOffPolicy);
//        retryTemplate.setRetryPolicy(getRetryPolicy());
//        return retryTemplate;
//    }
//
//    @Bean
//    public RetryPolicy getRetryPolicy() {
//        final HashMap<Class<? extends Throwable>, Boolean> errorMap = new HashMap<>();
//        errorMap.put(NullPointerException.class, true);
//        return new SimpleRetryPolicy(5, errorMap, true, true);
//    }

}

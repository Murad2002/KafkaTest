package com.example.kafkatest.config;

import com.example.kafkatest.domain.User;
import com.example.kafkatest.dto.TestDto;
import com.example.kafkatest.dto.UserRequestDto;
import com.example.kafkatest.repository.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class KafkaConsumer {
    private final ObjectMapper objectMapper;
    private final UserRepository userRepository;

    @KafkaListener(topics = "test1-topic", containerFactory = "kafkaListenerContainerFactoryCustom")
    public void listenToTopic(ConsumerRecord<String, String> dto) throws JsonProcessingException {
        var user = objectMapper.readValue(dto.value(), User.class);
        log.info("Message : {}", dto.value());
        if(5>3)
            throw new NullPointerException();
        userRepository.save(user);
        log.info("Message : {}", dto.value());
    }

//    @KafkaListener(topics = "test1-topic")
//    public void listenToTopic(ConsumerRecord<String, TestDto> dto) {
//        var a = dto.value();
//        log.info("Message : {}", dto.value());
//    }

//    @KafkaListener(topics = "test1-topic")
//    public void listenToTopic(TestDto dto) {
//        log.info("Message : {}", dto);
//    }

//    @KafkaListener(topics = "test1-topic")
//    public void listenToTopic(ConsumerRecord<String, UserRequestDto> dto) {
//        var a = dto.value();
//        log.info("Message : {}", dto.value());
//    }
}

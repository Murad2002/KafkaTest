//package com.example.kafkatest.config;
//
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.kafka.listener.ErrorHandler;
//import org.springframework.stereotype.Component;
//
//@Slf4j
//@Component
//@RequiredArgsConstructor
//public class KafkaErrorHandler implements ErrorHandler {
//
//    private final KafkaTemplate<String, Object> errorTemplate;
//    @Value("${spring.kafka.consumer.group-id}")
//    String groupId;
//
//    @Override
//    public void handle(Exception err, ConsumerRecord<?, ?> rec) {
//        final Throwable cause = err.getCause();
//        log.error("{}", cause.getMessage());
//
//        final KafkaErrorMessage<Object> kafkaErrorMessage = KafkaErrorMessage
//                .builder()
//                .data(rec.value())
//                .error(cause.getMessage())
//                .build();
//        errorTemplate.send(getTopic(rec), kafkaErrorMessage);
//    }
//
//    private String getTopic(ConsumerRecord<?, ?> rec) {
//        return String.format("%s_%s_ERROR", rec.topic(), groupId);
//    }
//
//    @Override
//    public boolean isAckAfterHandle() {
//        return true;
//    }
//}
